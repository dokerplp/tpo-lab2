package org.example.functions.trigonometry;

import org.example.functions.Computational;

public class Sec extends Computational {
    private final Cos cos = new Cos();

    @Override
    public double compute(final double x, final double accuracy) {
        return 1 / cos.compute(x, accuracy);
    }
}
