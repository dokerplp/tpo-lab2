package org.example.functions.trigonometry;

import org.example.functions.Computational;

public class Csc extends Computational {

    private final Sin sin = new Sin();

    @Override
    public double compute(final double x, final double accuracy) {
        return 1 / sin.compute(x, accuracy);
    }
}
