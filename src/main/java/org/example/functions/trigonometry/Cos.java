package org.example.functions.trigonometry;

import org.example.functions.Computational;

public class Cos extends Computational {
    private final Sin sin = new Sin();

    @Override
    public double compute(final double x, final double accuracy) {
        return sin.compute(Math.PI / 2 + x, accuracy);
    }


}
