package org.example.functions.trigonometry;

import org.example.functions.Computational;

public class Sin extends Computational {
    @Override
    public double compute(final double x, final double accuracy) {
        double acc = 0;
        int mul = 1;
        int pow = 1;
        double fact = 1;
        double p1 = Double.MAX_VALUE;
        double p2 = 0;
        while (Math.abs(p1 - p2) > accuracy) {
            acc += p2;
            p1 = p2;
            p2 = mul * Math.pow(x, pow) / fact;
            mul *= -1;
            fact *= ++pow;
            fact *= ++pow;
        }
        return acc;
    }
}
