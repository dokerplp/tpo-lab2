package org.example.functions.trigonometry;

import org.example.functions.Computational;

public class Cot extends Computational {

    private final Sin sin = new Sin();
    private final Cos cos = new Cos();

    @Override
    public double compute(final double x, final double accuracy) {
        return cos.compute(x, accuracy) / sin.compute(x, accuracy);
    }
}
