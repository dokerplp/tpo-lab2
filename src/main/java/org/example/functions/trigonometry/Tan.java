package org.example.functions.trigonometry;

import org.example.functions.Computational;

public class Tan extends Computational {
    private final Sin sin = new Sin();
    private final Cos cos = new Cos();

    @Override
    public double compute(final double x, final double accuracy) {
        return sin.compute(x, accuracy) / cos.compute(x, accuracy);
    }
}
