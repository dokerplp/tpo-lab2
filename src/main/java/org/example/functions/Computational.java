package org.example.functions;

import com.opencsv.CSVWriter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public abstract class Computational {
    abstract public double compute(double x, double accuracy);

    public void makeCsv(final String fileName, final List<Double> inputs, final double accuracy, final double limit) {
        final Path csv = Paths.get(fileName);
        try (
                BufferedWriter bw = Files.newBufferedWriter(csv);
                CSVWriter writer = new CSVWriter(bw, ',',
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END)
        ) {
            final String[] header = {"x", "y"};
            writer.writeNext(header);
            for (final double x : inputs) {
                final double y = compute(x, accuracy);
                if (Double.isFinite(y) && Math.abs(y) < limit) {
                    final String[] rec = {Double.toString(x), Double.toString(y)};
                    writer.writeNext(rec);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void rangeCsv(final String fileName, final double start, final double end, final double step, final double accuracy, final double limit) {
        final List<Double> inputs = new ArrayList<>();
        double cur = start;
        while (cur < end) {
            inputs.add(cur);
            cur += step;
        }
        makeCsv(fileName, inputs, accuracy, limit);
    }
}
