package org.example.functions.logarithm;

import org.example.functions.Computational;

public class Log extends Computational {

    private final int base;
    private final Ln ln = new Ln();

    public Log(final int base) {
        this.base = base;
    }

    @Override
    public double compute(final double x, final double accuracy) {
        return ln.compute(x, accuracy) / ln.compute(base, accuracy);
    }
}
