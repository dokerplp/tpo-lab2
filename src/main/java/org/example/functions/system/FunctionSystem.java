package org.example.functions.system;

import org.example.functions.Computational;
import org.example.functions.logarithm.Log;
import org.example.functions.trigonometry.*;

public class FunctionSystem extends Computational {

    private final Sin sinx = new Sin();
    private final Cos cosx = new Cos();
    private final Tan tanx = new Tan();
    private final Sec secx = new Sec();
    private final Csc cscx = new Csc();
    private final Cot cotx = new Cot();

    private final Log lgx = new Log(10);
    private final Log log2x = new Log(2);
    private final Log log3x = new Log(3);

    public double trigFunc(final double x, final double accuracy) {
        final double sin = sinx.compute(x, accuracy);
        final double cos = cosx.compute(x, accuracy);
        final double tan = tanx.compute(x, accuracy);
        final double sec = secx.compute(x, accuracy);
        final double csc = cscx.compute(x, accuracy);
        final double cot = cotx.compute(x, accuracy);

        final double f1 = Math.pow(cot, 3) / csc - cot - cot;
        final double f2 = (f1 - Math.pow(sec - csc, 3)) / cos;
        final double f3 = Math.pow(cot, 3) - (tan / (tan - sec));
        final double f4 = (f2 + sin - sin) * f3;
        final double f5 = f4 - (cos + cot + tan);
        return Math.pow(f5, 3);
    }

    public double logFunc(final double x, final double accuracy) {
        final double log2 = log2x.compute(x, accuracy);
        final double log3 = log3x.compute(x, accuracy);
        final double log10 = lgx.compute(x, accuracy);

        return (log2 / log3 * log3 * log2 - Math.pow(log10, 2)) / log2;
    }

    @Override
    public double compute(final double x, final double accuracy) {
        return x <= 0 ? trigFunc(x, accuracy) : logFunc(x, accuracy);
    }
}
