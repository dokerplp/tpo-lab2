package org.example;

import org.example.functions.logarithm.Ln;
import org.example.functions.system.FunctionSystem;
import org.example.functions.trigonometry.Cos;

import java.util.List;

import static java.lang.Math.PI;

public class Main {
    public static void main(final String[] args) {
        final Cos cos = new Cos();
        cos.rangeCsv("cos.csv", -10d, 10d, 0.01d, 0.000_01d, 10d);

        final Ln ln = new Ln();
        ln.rangeCsv("ln.csv", 0d, 10d, 0.01d, 0.000_01d, 10d);

        final FunctionSystem system = new FunctionSystem();
        system.rangeCsv("system.csv", -10d, 10d, 0.01d, 0.000_01d, 10d);
    }
}