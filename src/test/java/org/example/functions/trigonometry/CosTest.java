package org.example.functions.trigonometry;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CosTest {

    public static final Cos MOCKED_COS = new Cos();
    private final static double ACCURACY = 0.000_01;

    static {
        ReflectionTestUtils.setField(MOCKED_COS, "sin", SinTest.MOCKED_SIN);
    }

    private final Computational function = new Cos();

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0, 1d),
                Arguments.of(PI / 6, sqrt(3) / 2),
                Arguments.of(PI / 4, sqrt(2) / 2),
                Arguments.of(PI / 3, 1d / 2),
                Arguments.of(PI / 2, 0d),
                Arguments.of(2 * PI / 3, -1d / 2),
                Arguments.of(3 * PI / 4, -sqrt(2) / 2),
                Arguments.of(5 * PI / 6, -sqrt(3) / 2),

                Arguments.of(PI, -1d),
                Arguments.of(PI + PI / 6, -sqrt(3) / 2),
                Arguments.of(PI + PI / 4, -sqrt(2) / 2),
                Arguments.of(PI + PI / 3, -1d / 2),
                Arguments.of(PI + PI / 2, 0d),
                Arguments.of(PI + 2 * PI / 3, 1d / 2),
                Arguments.of(PI + 3 * PI / 4, sqrt(2) / 2),
                Arguments.of(PI + 5 * PI / 6, sqrt(3) / 2),

                Arguments.of(2 * PI, 1d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(1000d - PI / 2, 1000d),

                Arguments.of(0, 1d),
                Arguments.of(PI / 6, sqrt(3) / 2),
                Arguments.of(PI / 4, sqrt(2) / 2),
                Arguments.of(PI / 3, 1d / 2),
                Arguments.of(PI / 2, 0d),
                Arguments.of(2 * PI / 3, -1d / 2),
                Arguments.of(3 * PI / 4, -sqrt(2) / 2),
                Arguments.of(5 * PI / 6, -sqrt(3) / 2)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void cosTest(final double x, final double expected) {
        assertEquals(expected, function.compute(x, ACCURACY), ACCURACY);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void cosIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_COS.compute(x, ACCURACY), ACCURACY);
    }
}
