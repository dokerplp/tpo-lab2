package org.example.functions.trigonometry;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CscTest {
    public static final Csc MOCKED_CSC = new Csc();
    private static final double ACCURACY = 0.000_01;

    static {
        ReflectionTestUtils.setField(MOCKED_CSC, "sin", SinTest.MOCKED_SIN);
    }

    private final Computational function = new Csc();

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(PI / 6, 2d),
                Arguments.of(PI / 4, 2d / sqrt(2)),
                Arguments.of(PI / 3, 2d / sqrt(3)),
                Arguments.of(PI / 2, 1d),
                Arguments.of(2 * PI / 3, 2d / sqrt(3)),
                Arguments.of(3 * PI / 4, 2d / sqrt(2)),
                Arguments.of(5 * PI / 6, 2d),

                Arguments.of(PI + PI / 6, -2d),
                Arguments.of(PI + PI / 4, -2d / sqrt(2)),
                Arguments.of(PI + PI / 3, -2d / sqrt(3)),
                Arguments.of(PI + PI / 2, -1d),
                Arguments.of(PI + 2 * PI / 3, -2d / sqrt(3)),
                Arguments.of(PI + 3 * PI / 4, -2d / sqrt(2)),
                Arguments.of(PI + 5 * PI / 6, -2d),

                Arguments.of(2 * PI + PI / 2, 1d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(PI / 6, 2d),
                Arguments.of(PI / 4, 2d / sqrt(2)),
                Arguments.of(PI / 3, 2d / sqrt(3)),
                Arguments.of(PI / 2, 1d),
                Arguments.of(2 * PI / 3, 2d / sqrt(3)),
                Arguments.of(3 * PI / 4, 2d / sqrt(2)),
                Arguments.of(5 * PI / 6, 2d)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void cscTest(final double x, final double expected) {
        assertEquals(expected, function.compute(x, ACCURACY), ACCURACY);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void cscIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_CSC.compute(x, ACCURACY), ACCURACY);
    }
}
