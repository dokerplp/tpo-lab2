package org.example.functions.trigonometry;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CotTest {
    public static final Cot MOCKED_COT = new Cot();
    private static final double ACCURACY = 0.000_01;

    static {
        ReflectionTestUtils.setField(MOCKED_COT, "sin", SinTest.MOCKED_SIN);
        ReflectionTestUtils.setField(MOCKED_COT, "cos", CosTest.MOCKED_COS);
    }

    private final Computational function = new Cot();

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(PI / 6, sqrt(3)),
                Arguments.of(PI / 4, 1d),
                Arguments.of(PI / 3, 1d / sqrt(3)),
                Arguments.of(PI / 2, 0),
                Arguments.of(2 * PI / 3, -1d / sqrt(3)),
                Arguments.of(3 * PI / 4, -1d),
                Arguments.of(5 * PI / 6, -sqrt(3)),

                Arguments.of(PI + PI / 6, sqrt(3)),
                Arguments.of(PI + PI / 4, 1d),
                Arguments.of(PI + PI / 3, 1d / sqrt(3)),
                Arguments.of(PI + PI / 2, 0),
                Arguments.of(PI + 2 * PI / 3, -1d / sqrt(3)),
                Arguments.of(PI + 3 * PI / 4, -1d),
                Arguments.of(PI + 5 * PI / 6, -sqrt(3)),

                Arguments.of(2 * PI + PI / 2, 0d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(PI / 6, sqrt(3)),
                Arguments.of(PI / 4, 1d),
                Arguments.of(PI / 3, 1d / sqrt(3)),
                Arguments.of(PI / 2, 0),
                Arguments.of(2 * PI / 3, -1d / sqrt(3)),
                Arguments.of(3 * PI / 4, -1d),
                Arguments.of(5 * PI / 6, -sqrt(3))
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void cotTest(final double x, final double expected) {
        assertEquals(expected, function.compute(x, ACCURACY), ACCURACY);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void cotIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_COT.compute(x, ACCURACY), ACCURACY);
    }
}
