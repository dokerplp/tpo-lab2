package org.example.functions.trigonometry;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SecTest {
    public static final Sec MOCKED_SEC = new Sec();
    private static final double ACCURACY = 0.000_01;

    static {
        ReflectionTestUtils.setField(MOCKED_SEC, "cos", CosTest.MOCKED_COS);
    }

    private final Computational function = new Sec();

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0d, 1d),
                Arguments.of(PI / 6, 2d / sqrt(3)),
                Arguments.of(PI / 4, 2d / sqrt(2)),
                Arguments.of(PI / 3, 2d),
                Arguments.of(2 * PI / 3, -2d),
                Arguments.of(3 * PI / 4, -2d / sqrt(2)),
                Arguments.of(5 * PI / 6, -2d / sqrt(3)),

                Arguments.of(PI, -1d),
                Arguments.of(PI + PI / 6, -2d / sqrt(3)),
                Arguments.of(PI + PI / 4, -2d / sqrt(2)),
                Arguments.of(PI + PI / 3, -2d),
                Arguments.of(PI + 2 * PI / 3, 2d),
                Arguments.of(PI + 3 * PI / 4, 2d / sqrt(2)),
                Arguments.of(PI + 5 * PI / 6, 2d / sqrt(3)),

                Arguments.of(2 * PI, 1d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(0d, 1d),
                Arguments.of(PI / 6, 2d / sqrt(3)),
                Arguments.of(PI / 4, 2d / sqrt(2)),
                Arguments.of(PI / 3, 2d),
                Arguments.of(2 * PI / 3, -2d),
                Arguments.of(3 * PI / 4, -2d / sqrt(2)),
                Arguments.of(5 * PI / 6, -2d / sqrt(3))
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void secTest(final double x, final double expected) {
        assertEquals(expected, function.compute(x, ACCURACY), ACCURACY);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void secIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_SEC.compute(x, ACCURACY), ACCURACY);
    }
}
