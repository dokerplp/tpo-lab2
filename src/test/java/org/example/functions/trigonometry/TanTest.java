package org.example.functions.trigonometry;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TanTest {
    public static final Tan MOCKED_TAN = new Tan();
    private static final double ACCURACY = 0.000_01;

    static {
        ReflectionTestUtils.setField(MOCKED_TAN, "sin", SinTest.MOCKED_SIN);
        ReflectionTestUtils.setField(MOCKED_TAN, "cos", CosTest.MOCKED_COS);
    }

    private final Computational function = new Tan();

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0d, 0d),
                Arguments.of(PI / 6, 1d / sqrt(3)),
                Arguments.of(PI / 4, 1d),
                Arguments.of(PI / 3, sqrt(3)),
                Arguments.of(2 * PI / 3, -sqrt(3)),
                Arguments.of(3 * PI / 4, -1d),
                Arguments.of(5 * PI / 6, -1d / sqrt(3)),

                Arguments.of(PI, 0d),
                Arguments.of(PI + PI / 6, 1d / sqrt(3)),
                Arguments.of(PI + PI / 4, 1d),
                Arguments.of(PI + PI / 3, sqrt(3)),
                Arguments.of(PI + 2 * PI / 3, -sqrt(3)),
                Arguments.of(PI + 3 * PI / 4, -1d),
                Arguments.of(PI + 5 * PI / 6, -1d / sqrt(3)),

                Arguments.of(2 * PI, 0d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(0d, 0d),
                Arguments.of(PI / 6, 1d / sqrt(3)),
                Arguments.of(PI / 4, 1d),
                Arguments.of(PI / 3, sqrt(3)),
                Arguments.of(2 * PI / 3, -sqrt(3)),
                Arguments.of(3 * PI / 4, -1d),
                Arguments.of(5 * PI / 6, -1d / sqrt(3))
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void tanTest(final double x, final double expected) {
        assertEquals(expected, function.compute(x, ACCURACY), ACCURACY);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void tanIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_TAN.compute(x, ACCURACY), ACCURACY);
    }
}
