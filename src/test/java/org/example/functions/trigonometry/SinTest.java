package org.example.functions.trigonometry;

import org.example.functions.Computational;
import org.example.functions.DoubleMatcher;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SinTest {
    public static final Sin MOCKED_SIN;
    private static final double ACCURACY = 0.000_01;
    private static final double MOCK_ACCURACY = 0.1;

    static {
        MOCKED_SIN = mock(Sin.class);

        final List<List<Double>> args = new ArrayList<>(funcIntTestArgs()
                .map(arguments -> Arrays.copyOf(arguments.get(), 2, Double[].class))
                .map(Arrays::asList)
                .toList());

        args.addAll(List.of(
                List.of(-PI / 6, -1d / 2),
                List.of(-PI / 4, -sqrt(2) / 2),
                List.of(-PI / 3, -sqrt(3) / 2),
                List.of(-PI / 2, -1d),
                List.of(-2 * PI / 3, -sqrt(3) / 2),
                List.of(-3 * PI / 4, -sqrt(2) / 2),
                List.of(-5 * PI / 6, -1d / 2)
        ));

        for (final var params : args) {
            when(MOCKED_SIN.compute(match(params.get(0)), match(ACCURACY))).thenReturn(params.get(1));
        }
    }

    private final Computational function = new Sin();

    private static double match(final double value) {
        return doubleThat(new DoubleMatcher(value, MOCK_ACCURACY));
    }

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0, 0),
                Arguments.of(PI / 6, 1d / 2),
                Arguments.of(PI / 4, sqrt(2) / 2),
                Arguments.of(PI / 3, sqrt(3) / 2),
                Arguments.of(PI / 2, 1),
                Arguments.of(2 * PI / 3, sqrt(3) / 2),
                Arguments.of(3 * PI / 4, sqrt(2) / 2),
                Arguments.of(5 * PI / 6, 1d / 2),

                Arguments.of(PI, 0),
                Arguments.of(PI + PI / 6, -1d / 2),
                Arguments.of(PI + PI / 4, -sqrt(2) / 2),
                Arguments.of(PI + PI / 3, -sqrt(3) / 2),
                Arguments.of(PI + PI / 2, -1),
                Arguments.of(PI + 2 * PI / 3, -sqrt(3) / 2),
                Arguments.of(PI + 3 * PI / 4, -sqrt(2) / 2),
                Arguments.of(PI + 5 * PI / 6, -1d / 2),

                Arguments.of(2 * PI, 0)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(1000d, 1000d),

                Arguments.of(0d, 0d),
                Arguments.of(PI / 6, 1d / 2),
                Arguments.of(PI / 4, sqrt(2) / 2),
                Arguments.of(PI / 3, sqrt(3) / 2),
                Arguments.of(PI / 2, 1d),
                Arguments.of(2 * PI / 3, sqrt(3) / 2),
                Arguments.of(3 * PI / 4, sqrt(2) / 2),
                Arguments.of(5 * PI / 6, 1d / 2),

                Arguments.of(PI, 0d),
                Arguments.of(PI + PI / 6, -1d / 2),
                Arguments.of(PI + PI / 4, -sqrt(2) / 2),
                Arguments.of(PI + PI / 3, -sqrt(3) / 2),
                Arguments.of(PI + PI / 2, -1d),
                Arguments.of(PI + 2 * PI / 3, -sqrt(3) / 2),
                Arguments.of(PI + 3 * PI / 4, -sqrt(2) / 2),
                Arguments.of(PI + 5 * PI / 6, -1d / 2),

                Arguments.of(2 * PI, 0d)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void sinTest(final double x, final double expected) {
        assertEquals(expected, function.compute(x, ACCURACY), ACCURACY);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void cosIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_SIN.compute(x, ACCURACY), ACCURACY);
    }

}