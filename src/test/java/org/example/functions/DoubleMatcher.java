package org.example.functions;

import org.mockito.ArgumentMatcher;


public class DoubleMatcher implements ArgumentMatcher<Double> {

    private final double left;
    private final double accuracy;

    public DoubleMatcher(final double left, final double accuracy) {
        this.left = left;
        this.accuracy = accuracy;
    }

    private boolean matches(final double left, final double right, final double accuracy) {
        return Math.abs(left - right) < accuracy;
    }

    @Override
    public boolean matches(final Double right) {
        return matches(left, right, accuracy);
    }
}
