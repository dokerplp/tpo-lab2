package org.example.functions.system;

import org.example.functions.Computational;
import org.example.functions.logarithm.LgTest;
import org.example.functions.logarithm.Log2Test;
import org.example.functions.logarithm.Log3Test;
import org.example.functions.trigonometry.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.stream.Stream;

import static java.lang.Double.NaN;
import static java.lang.Math.PI;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

class FunctionSystemTest {
    public static final FunctionSystem MOCKED_SYSTEM = new FunctionSystem();
    private static final double ACCURACY_LOG = 0.000_000_000_01;
    private static final double ACCURACY_TRIG = 0.000_01;
    private static final double ACCURACY_TEST = 0.01;

    static {
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "sinx", SinTest.MOCKED_SIN);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "cosx", CosTest.MOCKED_COS);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "cotx", CotTest.MOCKED_COT);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "cscx", CscTest.MOCKED_CSC);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "secx", SecTest.MOCKED_SEC);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "tanx", TanTest.MOCKED_TAN);

        ReflectionTestUtils.setField(MOCKED_SYSTEM, "lgx", LgTest.MOCKED_LG);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "log2x", Log2Test.MOCKED_LOG2);
        ReflectionTestUtils.setField(MOCKED_SYSTEM, "log3x", Log3Test.MOCKED_LOG3);
    }

    private final Computational function = new FunctionSystem();

    static Stream<Arguments> trigFuncTestArgs() {
        return Stream.of(
                Arguments.of(-2.1, -10.646),
                Arguments.of(-2.098, -10.649),
                Arguments.of(-2.0962, -10.646),
                Arguments.of(-1.9714, 0),
                Arguments.of(-2.3177, 0),
                Arguments.of(-2.311, -0.002_69),
                Arguments.of(-2.3256, 0.005_11),
                Arguments.of(-1.9724, -0.000_139),
                Arguments.of(-1.9698, -0.000_54),

                Arguments.of(-3.8385, 0),
                Arguments.of(-5.796, 0),
                Arguments.of(-8.255, 0),
                Arguments.of(-8.601, 0),
                Arguments.of(-8.381, -10.649),

                Arguments.of(-10.122, 0),
                Arguments.of(-12.079, 0)
        );
    }

    static Stream<Arguments> logFuncTestArgs() {
        return Stream.of(
                Arguments.of(0.047, -4.011),
                Arguments.of(0.222, -1.975),
                Arguments.of(0.466, -1.002),
                Arguments.of(0.99, -0.013),
                Arguments.of(1.01, 0.013),
                Arguments.of(1.5, 0.532),
                Arguments.of(4.6, 2.002),
                Arguments.of(10.02, 3.024),

                Arguments.of(-2 * PI / 3, -10.635_7d),
                Arguments.of(-3 * PI / 4, 0.856_7d),

                Arguments.of(1, NaN),
                Arguments.of(0, NaN)
        );
    }

    static Stream<Arguments> logFuncIntTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -6.042d),
                Arguments.of(0.1d, -3.021d),
                Arguments.of(0.5d, -0.909d),
                Arguments.of(1.5d, 0.532d),
                Arguments.of(2d, 0.909d)
        );
    }

    static Stream<Arguments> trigFuncIntTestArgs() {
        return Stream.of(
                Arguments.of(-2 * PI / 3, -10.6357d),
                Arguments.of(-3 * PI / 4, 0.8567d)
        );
    }

    @ParameterizedTest
    @MethodSource("trigFuncTestArgs")
    void trigPartSystemTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> function.compute(x, ACCURACY_TRIG));
        assertEquals(expected, y, 0.01);
    }

    @ParameterizedTest
    @MethodSource("logFuncTestArgs")
    void logPartSystemTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> function.compute(x, ACCURACY_LOG));
        assertEquals(expected, y, 0.01);
    }

    @ParameterizedTest
    @MethodSource("logFuncIntTestArgs")
    void log2IntTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> MOCKED_SYSTEM.compute(x, ACCURACY_LOG));
        assertEquals(expected, y, ACCURACY_TEST);
    }

    @ParameterizedTest
    @MethodSource("trigFuncIntTestArgs")
    void cotIntTest(final double x, final double expected) {
        assertEquals(expected, MOCKED_SYSTEM.compute(x, ACCURACY_TRIG), ACCURACY_TRIG);
    }


}