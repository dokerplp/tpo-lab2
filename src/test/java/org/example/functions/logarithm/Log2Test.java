package org.example.functions.logarithm;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

public class Log2Test {
    public static final Log MOCKED_LOG2 = new Log(2);
    private static final double ACCURACY = 0.000_000_000_01;
    private static final double ACCURACY_TEST = 0.01;

    static {
        ReflectionTestUtils.setField(MOCKED_LOG2, "ln", LnTest.MOCKED_LN);
    }

    private final Computational function = new Log(2);

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -6.643_856d),
                Arguments.of(0.1d, -3.321_93d),
                Arguments.of(0.5d, -1d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.584_963d),
                Arguments.of(2d, 1d),

                Arguments.of(4d, 2d),
                Arguments.of(64d, 6d),
                Arguments.of(1024d, 10d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -6.643_856d),
                Arguments.of(0.1d, -3.321_93d),
                Arguments.of(0.5d, -1d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.584_963d),
                Arguments.of(2d, 1d)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void log2Test(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> function.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void log2IntTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> MOCKED_LOG2.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }
}
