package org.example.functions.logarithm;

import org.example.functions.Computational;
import org.example.functions.DoubleMatcher;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.Math.E;
import static java.lang.Math.pow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LnTest {

    public static final Ln MOCKED_LN;
    private static final double ACCURACY = 0.000_000_000_01;
    private static final double ACCURACY_TEST = 0.01;
    private static final double MOCK_ACCURACY = 0.001;

    static {
        MOCKED_LN = mock(Ln.class);

        final List<List<Double>> args = new ArrayList<>(funcIntTestArgs()
                .map(arguments -> Arrays.copyOf(arguments.get(), 2, Double[].class))
                .map(Arrays::asList)
                .toList());
        args.addAll(List.of(List.of(2d, 0.693_147d), List.of(3d, 1.098_612), List.of(10d, 2.302_585d)));

        for (final var params : args) {
            when(MOCKED_LN.compute(match(params.get(0)), match(ACCURACY))).thenReturn(params.get(1));
        }
    }

    private final Computational function = new Ln();

    private static double match(final double value) {
        return doubleThat(new DoubleMatcher(value, MOCK_ACCURACY));
    }

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -4.605_170d),
                Arguments.of(0.1d, -2.302_59d),
                Arguments.of(0.5d, -0.693_147d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.405_465d),
                Arguments.of(2d, 0.693_147d),

                Arguments.of(E, 1d),
                Arguments.of(pow(E, 2), 2d),
                Arguments.of(pow(E, 10), 10d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -4.605_170d),
                Arguments.of(0.1d, -2.302_59d),
                Arguments.of(0.5d, -0.693_147d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.405_465d),
                Arguments.of(2d, 0.693_147d)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void lnTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> function.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void lnIntTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> MOCKED_LN.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }
}