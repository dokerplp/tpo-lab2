package org.example.functions.logarithm;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

public class Log3Test {
    public static final Log MOCKED_LOG3 = new Log(3);
    private static final double ACCURACY = 0.000_000_000_01;
    private static final double ACCURACY_TEST = 0.01;

    static {
        ReflectionTestUtils.setField(MOCKED_LOG3, "ln", LnTest.MOCKED_LN);
    }

    private final Computational function = new Log(3);

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -4.191_807d),
                Arguments.of(0.1d, -2.095_90d),
                Arguments.of(0.5d, -0.630_930d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.369_070d),
                Arguments.of(2d, 0.630_929d),

                Arguments.of(3d, 1d),
                Arguments.of(9d, 2d),
                Arguments.of(243d, 5d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -4.191_807d),
                Arguments.of(0.1d, -2.095_90d),
                Arguments.of(0.5d, -0.630_930d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.369_070d),
                Arguments.of(2d, 0.630_929d)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void log3Test(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> function.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void log3IntTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> MOCKED_LOG3.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }
}
