package org.example.functions.logarithm;

import org.example.functions.Computational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

public class LgTest {
    public static final Log MOCKED_LG = new Log(10);
    private static final double ACCURACY = 0.000_000_000_01;
    private static final double ACCURACY_TEST = 0.01;

    static {
        ReflectionTestUtils.setField(MOCKED_LG, "ln", LnTest.MOCKED_LN);
    }

    private final Computational function = new Log(10);

    static Stream<Arguments> funcTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -2d),
                Arguments.of(0.1d, -1d),
                Arguments.of(0.5d, -0.301_030d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.176_091d),
                Arguments.of(2d, 0.301_029_9d),

                Arguments.of(10d, 1d),
                Arguments.of(100d, 2d),
                Arguments.of(100_000d, 5d)
        );
    }

    static Stream<Arguments> funcIntTestArgs() {
        return Stream.of(
                Arguments.of(0.01d, -2d),
                Arguments.of(0.1d, -1d),
                Arguments.of(0.5d, -0.301_030d),
                Arguments.of(1d, 0d),
                Arguments.of(1.5d, 0.176_091d),
                Arguments.of(2d, 0.301_029_9d)
        );
    }

    @ParameterizedTest
    @MethodSource("funcTestArgs")
    void lgTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> function.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }

    @ParameterizedTest
    @MethodSource("funcIntTestArgs")
    void lgIntTest(final double x, final double expected) {
        final double y = assertTimeoutPreemptively(Duration.ofSeconds(2), () -> MOCKED_LG.compute(x, ACCURACY));
        assertEquals(expected, y, ACCURACY_TEST);
    }
}
